<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Operadores logicos</title>
</head>
<body>
	<h1>Ejemplo de operaciones logicas en PHP</h1>
	<?php 

	$a = 8;
	$b = 3;
	$c = 3;

	echo ($a == $b) && ($c > $b),"<br>";
	echo ($a == $b) ||($c > $b),"<br>";
	echo !($b <= $c),"<br>";
	?>
	
	<!--Tabla de respuestas-->
	<h1>Tabla de respuestas</h1>
	<table border="1">
		<tr>
			<td align="center"> Pregunta</td>
			<td align="center"> Respuesta</td>
		</tr>
		<tr>
			<td align="center">&&</td>
			<td>Representa un Y logico, que compara que ambas condiciones sean verdaderas para devolver un valor de TRUE ó 1</td>
		</tr>
		<tr>
			<td align="center">||</td>
			<td>Representa un O logico, que compara y si una de las condiciones se cumple devuelve un TRUE ó 1</td>
		</tr>	
	</table>
</body>
</html>