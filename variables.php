<?php 

	$un_bool = true;			//valor booleano
	$un_str = "Programación";	//una cadena
	$un_str2 = 'Programación';	//una cadena
	$un_int = 12;				//un entero

	echo gettype($un_bool);		//imprime: Booleano
	echo "<br>";
	echo gettype($un_str); 		//imprime: string
	echo "<br>";
	//si el valor es entero, incrementara en 4
	if (is_int($un_int)) {
		$un_int += 4;
		echo $un_int;
		echo "<br>";
	}

	/*si $un_bool es una cadena, imprimirla
	sino no imprimira nada*/

	if (is_string($un_bool)) {
		echo "Cadena: $un_bool";
	}
	else{
		echo "No se puede imprimir";
	}

?>