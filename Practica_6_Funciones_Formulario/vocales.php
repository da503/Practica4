
<style>
	textarea {
    width: 50%;
    height: 150px;
    padding: 12px 20px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    background-color: #f8f8f8;
	}
	h1{
		
    	text-transform: uppercase;
    	color: #4CAF50;
	}
	h2{
		text-transform: uppercase;
    	color: #1FDC1A;
	}
	h3{
		text-indent: 50px;
    	text-align: justify;
    	letter-spacing: 3px;
    	font-size: 25px;
	}
	h4{
		text-indent: 50px;
    	text-align: justify;
    	letter-spacing: 3px;
    	font-size: 20px;
	}
	b{
		color: #FF0000;
	}
	input[type=submit] {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 2px;
    cursor: pointer;
	}

</style>
<center>
	<form action="" method="POST">
		<h1>Ingrese cualquier texto</h1>
		<textarea rows="5" cols="50" name="texto" autofocus></textarea><br><br>
		<input type="submit" value="Ver vocales" name="enviar">
	</form>
</center>


<?php 

	function extraeVocales($frase)
	{
		
		echo "	<h2>Texto ingresado</h2>
				<h3>$frase</h3>";

		$a = substr_count($frase, "a");
		$e = substr_count($frase, "e");
		$i = substr_count($frase, "i");
		$o = substr_count($frase, "o");
		$u = substr_count($frase, "u");

		$aa = substr_count($frase, "á");
		$ee = substr_count($frase, "é");
		$ii = substr_count($frase, "í");
		$oo = substr_count($frase, "ó");
		$uu = substr_count($frase, "ú");

		$A = substr_count($frase, "A") + $a + $aa;
		$E = substr_count($frase, "E") + $e + $ee;
		$I = substr_count($frase, "I") + $i + $ii;
		$O = substr_count($frase, "O") + $o + $oo;
		$U = substr_count($frase, "U") + $u + $uu;

		echo "<h2>Resultado</h2>";
		echo "<h4>El total de vocales <b>a</b> es: $A </h4>";
		echo "<h4>El total de vocales <b>e</b> es: $E </h4>";
		echo "<h4>El total de vocales <b>i</b> es: $I </h4>";
		echo "<h4>El total de vocales <b>o</b> es: $O </h4>";
		echo "<h4>El total de vocales <b>u</b> es: $U </h4>";
	}
	

	if (isset($_POST['enviar']) && $_POST['texto'] != "") {
		
		extraeVocales($_POST['texto']);
	}

 ?>