<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Uso del método GET</title>
</head>
<body>
	<h2>Conversor de Decimal a Binario</h2>
	<form name="frmpost" action="" method="GET">
		<input type="text" name="txtnumber" required>
		<input type="submit" name="btnok">
	</form>
	<?php 

		if (!empty($_GET['txtnumber'])) {
			$decimal = $_GET['txtnumber'];
			echo "El número decimal es: $decimal";

			$binario = ' ';
			do {
				$binario = $decimal % 2 . $binario;
				$decimal = (int) ($decimal/2);
			} while ($decimal > 0);
			echo "<br>Número Binario es: $binario";
		}

	 ?>
</body>
</html>