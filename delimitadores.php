<!DOCTYPE html>
<html>
<head>
	<title>Delimitadores de PHP</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/tabs.css">
	<link rel="stylesheet" type="text/css" href="css/delimeters.css">
</head>
<body>
	<header>
		<h1>Los delimitadores de PHP</h1>	
	</header>
	<section>
		<article>
			<?php 
				echo "<span class=\"diana\" id=\"una\"></span> \n";
				echo "<div class=\"tab\"> \n";
				echo "<a href=\"#una\" class=\"tab-e\"> Estilo XML </a> \n";
				echo "< div class=\"first\"> \n";
				echo "<p class=\"xmltag\"> \n";
				echo "Este texto esta escrito en PHP, utilizando las etiquetas mas";
				echo "usuales y recomendas para delimitar el codigo PHP, que son: ";
				echo "&lt;?php ... ?&gt;.<br>\n";
				echo "<p>\n";
				echo "<div>\n";
				echo "<div>\n";
			 ?>
			 <script language="php">
			 	echo "<span class=\"diana\" id=\"dos\"></span> \n";
				echo "<div class=\"tab\"> \n";
				echo "<a href=\"#dos\" class=\"tab e\">Script</a> \n";
				echo "<div>\n"
				echo "<p class=\"htmltag\"> \n";
				echo "Apesar que estas lineas estan escritas en un script PHP,";
				echo "estan enmarcadas dentro de etiquetas HTML:";
				echo "&lt;script&gt;...&lt;/script&gt;";
				echo "<p>\n";
				echo "<div>\n";
				echo "<div>\n";
			 </script>
			 <?
			 	echo "<span class=\"diana\" id=\"tres\"></span> \n";
				echo "<div class=\"tab\"> \n";
				echo "<a href=\"#tres\" class=\"tab-e\">Etiquetas cortas </a> \n";
				echo "< div> \n";
				echo "<p class=\"shorttag\"> \n";
				echo "Este texto tambien esta escrito en PHP, ";
				echo "utilizando etiquetas cortas: ";
				echo "cortas,<br>\n que son: &lt;?...?&gt;";
				echo "<p>\n";
				echo "<div>\n";
				echo "<div>\n";	
			 ?>
			 <%
			 	echo "<span class=\"diana\" id=\"cuatro\"></span> \n";
				echo "<div class=\"tab\"> \n";
				echo "<a href=\"#cuatro\" class=\"tab-e\">Estilo ASP </a> \n";
				echo "< div> \n";
				echo "<p class=\"asptag\"> \n";
				echo "Este texto tambien esta escrito en PHP, ";
				echo "con delimitaciones ASP: ";
				echo "&lt;%... %&gt;.<br>\n";
				echo "<p>\n";
				echo "<div>\n";
				echo "<div>\n";
			 %>
		</article>
	</section>
</body>
</html>