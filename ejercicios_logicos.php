<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Operaciones logicas ejercicios</title>
</head>
<body>
	<h2>Resolver las siguientes expresiones</h2>
	<h3>i = 9</h3>
	<h3>f = 33.5</h3>
	<h3>c = 'x'</h3>

	<h2>Tabla de solución</h2>
	<table border="1">
		<tr>
			<td align="center">Expresión</td>
			<td align="center">Resultado</td>
		</tr>
		<tr>
			<td>(i >= 6) && (c == X)</td>
			<td>TRUE</td>
		</tr>
		<tr>
			<td>(i >= 6) || (c == 12)</td>
			<td>TRUE</td>
		</tr>
		<tr>
			<td>(f < 11) && (i > 100)</td>
			<td>FALSE</td>
		</tr>
		<tr>
			<td>(c != p) || ((i + f) <=10)</td>
			<td>TRUE</td>
		</tr>
		<tr>
			<td>i + f <= 10</td>
			<td>FALSE</td>
		</tr>
		<tr>
			<td>i >= 6 && c == x</td>
			<td>TRUE</td>
		</tr>
		<tr>
			<td>c != p || i + f <=10</td>
			<td>TRUE</td>
		</tr>
	</table>



</body>
</html>