<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Operadores de comparacion</title>
</head>
<body>
	<h1>Ejemplo de operaciones comparaciones en PHP</h1>
	<?php
		$a = 8;
		$b = 3;
		$c = 3;
		echo $a == $b, "<br>";
		echo $a != $b, "<br>";
		echo $a < $b, "<br>";
		echo $a > $b, "<br>";
		echo $a <= $c, "<br>";
		echo $a >= $c, "<br>";
	?>
	<!--Tabla de resultados-->
	<h1>Tabla de respuestas</h1>
	<table border="1">
			<tr>
				<th scope="col" align="center">Pregunta</th>
				<th scope="col"> Respuesta</th>
			</tr>
			<tr>
				<td align="center" >==</td>
				<td >se utiliza para definir igualdad entre variables</td>
			</tr>
			<tr>
				<td align="center">!=</td>
				<td>se utiliza para decir que es diferente a otra variable </td>	
			</tr>
			<tr>
				<td align="center"> < </td>
				<td> se utiliza para decir que es menor que otra variable</td>
			</tr>
			<tr>
				<td align="center"> > </td>
				<td> se utiliza para decir que es mayor a otra variable</td>
			</tr>
			<tr>
				<td align="center"> <= </td>
				<td> se utiliza para decir que es menor o igual a otra variable</td>
			</tr>
			<tr>
				<td align="center"> >= </td>
				<td> se utiliza para decir que es mayor o igual a otra variable </td>
			</tr>

	</table>
</body>
</html>