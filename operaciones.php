<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Ejemplo de operaciones</title>
</head>
<body>
	<h1>Ejemplos de operaciones aritmeticas en PHP</h1>
	<?php 

	$a = 5;
	$b = 15;

	echo $a + $b, "<br>";
	echo $a - $b, "<br>";
	echo $a * $b, "<br>";
	echo $a / $b, "<br>";
	$a++;
	echo $a, "<br>";
	$b--;
	echo $b,"<br>";
	?>
</body>
</html>